///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>

#include "animals.h"
#include "cat.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   switch(color) {
      case BLACK : return "Black";
      case WHITE : return "White";
      case RED   : return "Red";
      case BLUE  : return "Blue";
      case GREEN : return "Green";
      case PINK  : return "Pink";
   }

   return NULL; // We should never get here
}

/// Decode the enum Gender into strings for printf()
char* genderName (enum Gender gender) {

   // @todo Map the enum Gender to a string
   switch(gender) {
      case MALE   : return "Male";
      case FEMALE : return "Female";
   }

   return NULL; // We should never get here
}

/// Decode the boolean isFixed into strings for printf()
char* fixedStatus (bool isFixed) {

   // @todo Map the boolean isFixed to a string
   switch(isFixed) {
      case true   : return "Yes";
      case false  : return "No";
   }

   return NULL; // We should never get here
}

