///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Gender {
   MALE,
   FEMALE
};

enum Color;

/// Return a string for the name of the color
char* colorName (enum Color color);

/// Return a string for the gender
char* genderName (enum Gender gender);

/// Return a string for isFixed
char* fixedStatus (bool isFixed);
