///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animals.h"

enum Color {
   BLACK,
   WHITE,
   RED,
   BLUE,
   GREEN,
   PINK
};

enum CatBreeds {
   MAIN_COON,
   MANX,
   SHORTHAIR,
   PERSIAN,
   SPHYNX
};


/// @todo declare a struct Cat here
struct Cat {
   char name[30];
   enum Gender gender;
   enum CatBreeds catbreeds;
   bool isFixed;
   float weight;
   enum Color collar1_color;
   enum Color collar2_color;
   long license;
};

char* breedName (enum CatBreeds catbreeds);

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) ;



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) ;

