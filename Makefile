###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Eliya Nakamura <eliyanak@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   2 February 2021
###############################################################################

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"
TARGET = animalfarm

all: $(TARGET)

main.o: main.c animals.h cat.h
	$(CC) $(CFLAGS) -c main.c

animals.o: animals.c animals.h cat.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c animals.h cat.h
	$(CC) $(CFLAGS) -c cat.c

animalfarm: main.o animals.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o animals.o cat.o

clean:
	rm -f *.o animalfarm
