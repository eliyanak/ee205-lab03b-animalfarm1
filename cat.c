/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"
#include "animals.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   struct Cat alice;
   strcpy(alice.name, "Alice");
   alice.gender = FEMALE;
   alice.catbreeds = MAIN_COON;
   alice.isFixed = true;
   alice.weight = 12.34;
   alice.collar1_color = BLACK;
   alice.collar2_color = RED;
   alice.license = 12345;
   catDB[i] = alice;
}

/// Decode the enum CatBreeds into strings for printf()
char* breedName (enum CatBreeds catbreeds) {

   // @todo Map the enum CatBreed to a string
   switch(catbreeds) {
      case MAIN_COON : return "Main Coon";
      case MANX      : return "Manx";
      case SHORTHAIR : return "Shorthair";
      case PERSIAN   : return "Persian";
      case SPHYNX    : return "Sphynx";
   }

   return NULL; // We should never get here
}

/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   printf("Cat name = [%s]\n", catDB[i].name);
   printf("    gender = [%s]\n", genderName( catDB[i].gender ));
   printf("    breed = [%s]\n", breedName( catDB[i].catbreeds ));
   printf("    isFixed = [%s]\n", fixedStatus( catDB[i].isFixed ));
   printf("    weight = [%.2f]\n", catDB[i].weight);
   printf("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf("    collar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf("    license = [%ld]\n", catDB[i].license);
}

